The thmbox package
==================

A LaTeX package that provides a fancy formatting of theorem-like
environments with boxes around them, and a few auxiliary features.

Installation
------------

To extract the package file, just type

	latex thmbox.ins

which will produce the file thmbox.sty. Once this is done, you can get
documentation by compiling the source using

	latex thmbox.dtx

Alternatively, the file `build.lua` is provided for use with the
[LaTeX3 build system](https://www.ctan.org/pkg/l3build).

License
-------

Copyright 2001-2022 Emmanuel Beffara <manu@beffara.org>

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
 <http://www.latex-project.org/lppl.txt>
and version 1.3 or later is part of all distributions of LaTeX
version 2005/12/01 or later.

This work has the LPPL maintenance status “maintained”.

The Current Maintainer of this work is Emmanuel Beffara.

This work consists of the file `thmbox.dtx`.
